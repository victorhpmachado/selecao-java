package com.selecaojava;

import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.selecaojava.model.Usuario;
import com.selecaojava.repository.UsuarioRepository;

@SpringBootApplication
public class SelecaoJavaApplication {

	@Bean
	CommandLineRunner criaUsuarios( UsuarioRepository usuarioRepository ){
		return args -> {
			
			Usuario user = new Usuario();
			user.setNome("Admin");
			user.setEmail("admin@com.br");
			user.setPassword("12345");
			user.setDtCriacao(new Date());
			user.setStatus(1);
			user.setPrivilegiado(1);
			
			usuarioRepository.save(user);	
			
		};

	}
	
	public static void main(String[] args) {
		SpringApplication.run(SelecaoJavaApplication.class, args);
	}

}

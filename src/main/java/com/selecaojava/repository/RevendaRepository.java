package com.selecaojava.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.selecaojava.model.Revenda;

@Repository
public interface RevendaRepository extends JpaRepository<Revenda, Long>{

	Revenda findByCnpj(String cnpj);

}

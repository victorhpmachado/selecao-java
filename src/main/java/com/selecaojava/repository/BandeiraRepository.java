package com.selecaojava.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.selecaojava.model.Bandeira;

@Repository
public interface BandeiraRepository extends JpaRepository<Bandeira, Long>{

	Bandeira findByDescricao(String descricao);

}

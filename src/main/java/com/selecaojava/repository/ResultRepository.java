package com.selecaojava.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.selecaojava.model.Coleta;

@Repository
public interface ResultRepository extends JpaRepository<Coleta, Long>{

	@Query(value="select avg(convert(valor_venda , decimal(10,3))) as media, municipio.descricao \r\n" + 
			"from coleta coleta\r\n" +
			"join revenda revenda on revenda.id = coleta.revenda_id \r\n" +
			"join municipio municipio on municipio.id = revenda.municipio_id \r\n" +
			"where municipio.descricao = :municipio group by municipio.descricao ", nativeQuery = true)
	List<Map<String, String>> listMedia(@Param("municipio") String municipio);

}

package com.selecaojava.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.selecaojava.model.Municipio;

@Repository
public interface MunicipioRepository extends JpaRepository<Municipio, Long>{

	Municipio findByMunicipio(String sigla);

}

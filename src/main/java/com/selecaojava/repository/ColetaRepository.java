package com.selecaojava.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.selecaojava.model.Coleta;
import com.selecaojava.model.Produto;
import com.selecaojava.model.Revenda;

@Repository
public interface ColetaRepository extends JpaRepository<Coleta, Long>{

	Coleta findByRevendaAndProdutoAndData(Revenda revenda, Produto produto, String data);

}

package com.selecaojava.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;


@Entity
public class Usuario  extends BaseEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name = "nome")
	private String nome;
	private String email;
	private String password;
	private Date dtCriacao;
	private Date dtAtualizacao;
	private int status;
	private int privilegiado;


	public Usuario() {
	}
	
	
	public Usuario(String nome, String email, String password, Date dtCriacao, Date dtAtualizacao, int status) {
		this.nome = nome;
		this.email = email;
		this.password = password;
		this.dtCriacao = dtCriacao;
		this.dtAtualizacao = dtAtualizacao;
		this.status = status;
	}
	
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getPrivilegiado() {
		return privilegiado;
	}

	public void setPrivilegiado(int privilegiado) {
		this.privilegiado = privilegiado;
	}


	
}

package com.selecaojava.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Municipio extends BaseEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String municipio;
	private Estado estado;
	
	public Municipio() {
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	
	@ManyToOne
	@JoinColumn(name = "estado_id")
	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	
	
	

}

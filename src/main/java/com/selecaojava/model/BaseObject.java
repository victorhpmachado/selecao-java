package com.selecaojava.model;

import java.io.Serializable;

public interface BaseObject extends Serializable {
	
	public Long getId();
	public void setId(Long id);
}

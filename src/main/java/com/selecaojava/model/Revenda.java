package com.selecaojava.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Revenda extends BaseEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String razaoSocial;
	private String cnpj;
	private Municipio municipio;
	private Bandeira bandeira;
	
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	

	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	@ManyToOne
	@JoinColumn(name = "municipio_id")
	public Municipio getMunicipio() {
		return municipio;
	}
	
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	
	@ManyToOne
	@JoinColumn(name = "bandeira_id")
	public Bandeira getBandeira() {
		return bandeira;
	}
	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}
	
	
	
	
	
	
}

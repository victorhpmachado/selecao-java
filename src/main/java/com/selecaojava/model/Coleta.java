package com.selecaojava.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Coleta extends BaseEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Revenda revenda;
	private Produto produto;
	private String data;
	private String valorVenda;
	private String valorCompra;
	private UnidadeMedida unidadeMedida;
	
	
	public Coleta() {
	}

	public Coleta(Revenda revenda, Produto produto, String data, String valorVenda, String valorCompra,
			UnidadeMedida unidadeMedida) {
		super();
		this.revenda = revenda;
		this.produto = produto;
		this.data = data;
		this.valorVenda = valorVenda;
		this.valorCompra = valorCompra;
		this.unidadeMedida = unidadeMedida;
	}


	@ManyToOne
	@JoinColumn(name = "revenda_id")
	public Revenda getRevenda() {
		return revenda;
	}
	public void setRevenda(Revenda revenda) {
		this.revenda = revenda;
	}
	
	@ManyToOne
	@JoinColumn(name = "produto_id")
	public Produto getProduto() {
		return produto;
	}
	
	public String getData() {
		return data;
	}



	public void setData(String data) {
		this.data = data;
	}



	public String getValorVenda() {
		return valorVenda;
	}






	public void setValorVenda(String valorVenda) {
		this.valorVenda = valorVenda;
	}






	public String getValorCompra() {
		return valorCompra;
	}






	public void setValorCompra(String valorCompra) {
		this.valorCompra = valorCompra;
	}






	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}
	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	
	
	
	
	

}

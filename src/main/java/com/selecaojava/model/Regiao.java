package com.selecaojava.model;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Regiao extends BaseEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String sigla;
	
	public Regiao() {
	}
	
	
	
	public Regiao(String sigla) {
		super();
		this.sigla = sigla;
	}



	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	

	

}

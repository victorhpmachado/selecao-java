package com.selecaojava.model;

public enum UnidadeMedida {
	
	RL("R$ / litro");
	
	private String unidadeMedida;
	
	private UnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	

}

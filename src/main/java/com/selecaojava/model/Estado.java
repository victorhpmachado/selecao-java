package com.selecaojava.model;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Estado extends BaseEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String sigla;
	private Regiao regiao;
	
	public Estado() {
		
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	


}

package com.selecaojava.model;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Produto extends BaseEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}

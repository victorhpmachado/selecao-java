package com.selecaojava.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.selecaojava.service.ResultService;

@RestController
@RequestMapping("/api/result")
public class ResultController {
	
	@Autowired
	private ResultService resultService;
	

	@GetMapping(value = "/mediaPrecoPorMunicipo", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Map<String, String>> mediaPrecoPorMunicipo(@RequestParam("municipio") String municipio) {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		result = resultService.listMedia(municipio);
		return result;
	}
	
}

package com.selecaojava.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.selecaojava.model.Usuario;
import com.selecaojava.service.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
	
	
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping(value = "/usuarios", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Usuario> ListUsuarios() {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		usuarios = usuarioService.ListUsuarios();
		return usuarios;
	}

	@GetMapping(value = "/getUsuario/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<Usuario> getUsuario(@PathVariable Long id) {
		Optional<Usuario> usuarioAlterado = usuarioService.getUsuario(id);		
		return usuarioAlterado;
	}
	
	@PutMapping(value = "/updateUsuario")
	public Usuario updateUsuario(@RequestBody Usuario usuario) {
		Usuario usuarioAlterado = usuarioService.inserir(usuario);		
		return usuarioAlterado;
	}
	
	@PostMapping(value = "/saveUsuario")
	public Usuario saveUsuario(@RequestBody Usuario usuario) {
		Usuario usuarioAlterado = usuarioService.inserir(usuario);		
		return usuarioAlterado;
	}
	
	@DeleteMapping(value = "/removeUsuario/{id}")
	public void removeUsuario(@PathVariable Long id) {
		usuarioService.remover(id);
	}	

}

package com.selecaojava.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import com.selecaojava.model.Arquivo;
import com.selecaojava.model.Bandeira;
import com.selecaojava.model.Coleta;
import com.selecaojava.model.Estado;
import com.selecaojava.model.Municipio;
import com.selecaojava.model.Produto;
import com.selecaojava.model.Regiao;
import com.selecaojava.model.Revenda;
import com.selecaojava.model.UnidadeMedida;
import com.selecaojava.service.BandeiraService;
import com.selecaojava.service.ColetaService;
import com.selecaojava.service.EstadoService;
import com.selecaojava.service.MunicipioService;
import com.selecaojava.service.ProdutoService;
import com.selecaojava.service.RegiaoService;
import com.selecaojava.service.RevendaService;

@RestController
@RequestMapping("/api/arquivo")
public class UploadController {

	@Autowired
	private BandeiraService bandeiraService;
	
	@Autowired
	private ColetaService coletaService;
	
	@Autowired
	private EstadoService estadoService;
	
	@Autowired
	private MunicipioService municipioService;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private RevendaService revendaService;
	
	@Autowired
	private RegiaoService regiaoService;
	
	
	
	@GetMapping("/index")
	public String index() {
		return "index";
	}

	@PostMapping("/upload-csv-file")
	public String uploadCSVFile(@RequestParam("file") MultipartFile file, Model model) {

		if (file.isEmpty()) {
			return "Selecione um arquivo";

		} else {

			try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream(),StandardCharsets.UTF_8))) {

					CSVParser parser = new CSVParserBuilder()
				        .withSeparator('\t')
				        .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_QUOTES)
				        .withIgnoreLeadingWhiteSpace(true)
				        .build();

				CSVReader csvReader = new CSVReaderBuilder(reader)
				        .withSkipLines(1)
				        .withCSVParser(parser)
				        .build();
			
				   List<String[]> list = new ArrayList<>();
				    
				    List<Arquivo> arquivos = new ArrayList<>();
				    
				    list = csvReader.readAll();

					for (String[] strings : list) {
						
						if(strings.length > 1) {
							Arquivo arquivo = new Arquivo(
									strings[0],
									strings[1],
									strings[2],
									strings[3],
									strings[4],
									strings[5],
									strings[6],
									strings[7],
									strings[8],
									strings[9],
									strings[10]
							);
							arquivos.add(arquivo);
						}
					}
				    csvReader.close();
				    
				    if (arquivos.size() > 0){
				    	persistirDados(arquivos);
				    }else {
				    	return "Arquivo sem dados";
				    }


			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return "ÖK";
	}
	
	@SuppressWarnings("null")
	public void persistirDados(List<Arquivo> arquivos) throws ParseException {
		

		for (Arquivo arquivo : arquivos) {
			
			Estado estado = new Estado();
			Regiao regiao = new Regiao();
			Municipio municipio = new Municipio();
			Revenda revenda = new Revenda();
			Bandeira bandeira = new Bandeira();
			Produto produto = new Produto();
			Coleta coleta = new Coleta();
			
			bandeira = bandeiraService.findByDescricao(arquivo.getBandeira());
			
			if(bandeira == null) {
				bandeira = new Bandeira();
				bandeira.setDescricao(arquivo.getBandeira());
				bandeira = bandeiraService.inserir(bandeira);
			}
			
			regiao = regiaoService.findBySigla(arquivo.getRegiao());

			if(regiao == null) {
				regiao = new Regiao(arquivo.getRegiao());
				regiao = regiaoService.inserir(regiao);
			}
			
			estado = estadoService.findBySigla(arquivo.getEstado());
			
			if (estado == null) {
				estado = new Estado();
				estado.setSigla(arquivo.getEstado());
				estado.setRegiao(regiao);
				estado = estadoService.inserir(estado);
			}
			
			municipio = municipioService.findByMunicipio(arquivo.getMunicipio());
			
			if (municipio == null) {
				municipio = new Municipio();
				municipio.setEstado(estado);
				municipio.setMunicipio(arquivo.getMunicipio());
				municipio = municipioService.inserir(municipio);
			}
			
			revenda = revendaService.findByCnpj(arquivo.getCnpj());
			
			if (revenda == null) {
				revenda = new Revenda();
				revenda.setCnpj(arquivo.getCnpj());
				revenda.setRazaoSocial(arquivo.getRevenda());
				revenda.setMunicipio(municipio);
				revenda.setBandeira(bandeira);

				revenda = revendaService.inserir(revenda);
			}
			
			
			produto = produtoService.findByDescricao(arquivo.getProduto());
			
			if (produto == null) {
				produto = new Produto();
				produto.setDescricao(arquivo.getProduto());
				produto = produtoService.inserir(produto);
			}

			coleta = coletaService.findByRevendaAndProdutoAndData(revenda, produto, arquivo.getDataColeta());
					
			if (coleta == null) {
				coleta = new Coleta(revenda, produto, arquivo.getDataColeta(), arquivo.getValorVenda(), arquivo.getValorVenda(), UnidadeMedida.RL);
				coleta = coletaService.inserir(coleta);
			}
			
		}
	

	}
}

package com.selecaojava.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecaojava.repository.ResultRepository;


@Service
public class ResultService {
	
	@Autowired
	ResultRepository resultRepository;
	
	public List<Map<String, String>> listMedia( String municipio) {
		return resultRepository.listMedia(municipio);
	}
	
}

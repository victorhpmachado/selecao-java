package com.selecaojava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecaojava.model.Regiao;
import com.selecaojava.repository.RegiaoRepository;


@Service
public class RegiaoService {
	
	@Autowired
	RegiaoRepository regiaoRepository;
	
	public Optional<Regiao> findById(Long id) {
		return regiaoRepository.findById(id);
	}	
	
	public Regiao findBySigla(String sigla) {
		return regiaoRepository.findBySigla(sigla);
	}	

	public List<Regiao> List() {
		return regiaoRepository.findAll();
	}

	public Regiao inserir(Regiao regiao) {
		return regiaoRepository.save(regiao);
	}

	public void remover(Long id) {
		regiaoRepository.deleteById(id);
	}
	
}

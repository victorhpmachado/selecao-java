package com.selecaojava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecaojava.model.Revenda;
import com.selecaojava.repository.RevendaRepository;


@Service
public class RevendaService {
	
	@Autowired
	RevendaRepository revendaRepository;
	
	
	public Optional<Revenda> findById(Long id) {
		return revendaRepository.findById(id);
	}	
	
	public Revenda findByCnpj(String cnpj) {
		return revendaRepository.findByCnpj(cnpj);
	}	

	public List<Revenda> ListE() {
		return revendaRepository.findAll();
	}

	public Revenda inserir(Revenda revenda) {
		return revendaRepository.saveAndFlush(revenda);
	}

	public void remover(Long id) {
		revendaRepository.deleteById(id);
	}
}

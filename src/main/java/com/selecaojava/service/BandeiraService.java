package com.selecaojava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecaojava.model.Bandeira;
import com.selecaojava.repository.BandeiraRepository;


@Service
public class BandeiraService {
	
	@Autowired
	BandeiraRepository bandeiraRepository;
	
	public Optional<Bandeira> findById(Long id) {
		return bandeiraRepository.findById(id);
	}	
	
	public Bandeira findByDescricao(String descricao) {
		return bandeiraRepository.findByDescricao(descricao);
	}	

	public List<Bandeira> ListE() {
		return bandeiraRepository.findAll();
	}

	public Bandeira inserir(Bandeira bandeira) {
		return bandeiraRepository.saveAndFlush(bandeira);
	}

	public void remover(Long id) {
		bandeiraRepository.deleteById(id);
	}
	
}

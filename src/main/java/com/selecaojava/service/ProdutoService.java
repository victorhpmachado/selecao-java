package com.selecaojava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecaojava.model.Produto;
import com.selecaojava.repository.ProdutoRepository;


@Service
public class ProdutoService {
	
	@Autowired
	ProdutoRepository produtoRepository;
	
	public Optional<Produto> findById(Long id) {
		return produtoRepository.findById(id);
	}	
	
	public Produto findByDescricao(String descricao) {
		return produtoRepository.findByDescricao(descricao);
	}	

	public List<Produto> List() {
		return produtoRepository.findAll();
	}

	public Produto inserir(Produto produto) {
		return produtoRepository.saveAndFlush(produto);
	}

	public void remover(Long id) {
		produtoRepository.deleteById(id);
	}
	
}

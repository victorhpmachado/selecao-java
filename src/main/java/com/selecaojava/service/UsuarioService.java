package com.selecaojava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecaojava.model.Usuario;
import com.selecaojava.repository.UsuarioRepository;


@Service
public class UsuarioService {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	public Optional<Usuario> getUsuario(Long id) {
		return usuarioRepository.findById(id);
	}	

	public List<Usuario> ListUsuarios() {
		return usuarioRepository.findAll();
	}

	public Usuario inserir(Usuario usuario) {
		return usuarioRepository.saveAndFlush(usuario);
	}

	public void remover(Long id) {
		usuarioRepository.deleteById(id);
	}

}

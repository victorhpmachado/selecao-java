package com.selecaojava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecaojava.model.Estado;
import com.selecaojava.repository.EstadoRepository;


@Service
public class EstadoService {
	
	@Autowired
	EstadoRepository estadoRepository;
	
	public Optional<Estado> findById(Long id) {
		return estadoRepository.findById(id);
	}	
	
	public Estado findBySigla(String sigla) {
		return estadoRepository.findBySigla(sigla);
	}	

	public List<Estado> ListEstados() {
		return estadoRepository.findAll();
	}

	public Estado inserir(Estado estado) {
		return estadoRepository.saveAndFlush(estado);
	}

	public void remover(Long id) {
		estadoRepository.deleteById(id);
	}
	
}

package com.selecaojava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecaojava.model.Coleta;
import com.selecaojava.model.Produto;
import com.selecaojava.model.Revenda;
import com.selecaojava.repository.ColetaRepository;


@Service
public class ColetaService {
	
	@Autowired
	ColetaRepository coletaRepository;
	
	public Optional<Coleta> findById(Long id) {
		return coletaRepository.findById(id);
	}	
	

	public Coleta findByRevendaAndProdutoAndData(Revenda revenda,Produto produto,String data) {
		return coletaRepository.findByRevendaAndProdutoAndData( revenda, produto, data);
	}
	
	public List<Coleta> list() {
		return coletaRepository.findAll();
	}

	public Coleta inserir(Coleta coleta) {
		return coletaRepository.saveAndFlush(coleta);
	}

	public void remover(Long id) {
		coletaRepository.deleteById(id);
	}
	
}

package com.selecaojava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecaojava.model.Municipio;
import com.selecaojava.repository.MunicipioRepository;


@Service
public class MunicipioService {
	
	@Autowired
	MunicipioRepository municipioRepository;
	
	public Optional<Municipio> findById(Long id) {
		return municipioRepository.findById(id);
	}	
	
	public Municipio findByMunicipio(String municipio) {
		return municipioRepository.findByMunicipio(municipio);
	}	

	public List<Municipio> ListE() {
		return municipioRepository.findAll();
	}

	public Municipio inserir(Municipio municipio) {
		return municipioRepository.saveAndFlush(municipio);
	}

	public void remover(Long id) {
		municipioRepository.deleteById(id);
	}
	
}
